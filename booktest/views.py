from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.

def index(request):
    #return HttpResponse('<h1>Hello World</h1>')

    #1、定义上下文   123
    context = {}
    #2、渲染
    return render(request,'booktest/index.html',context)
